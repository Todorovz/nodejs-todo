const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const app = express();
const port = 3001;
const User = require('./model/user');
const Todo = require('./model/todoList');

mongoose.connect('mongodb://localhost/todo_db', {useNewUrlParser: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    // we're connected!
    app.use(cors());
    app.use(bodyParser.json());

    app.post('/user', (req, res) => {
        let user = new User(req.body);
        user.save((err) => {
            if (err) {
                res.status(500);
                res.end();
            } else {
                res.json(user);
                res.end();
            }
        })
    });

    app.post('/user/authorization', (req, res) => {
       User.findOne({email: req.body.email})
           .select("+password")
           .then((user) => {
               if (user) {
                   if (req.body.password === user.password) {
                       jwt.sign({userId: user._id}, 'TodoAppv1!', (err, token) => {
                           if (err) {
                               console.error(err);
                               res.status(500);
                               res.end();
                           } else {
                               if (token) {
                                   user.jwtToken = token;
                                   user.save((err) => {
                                       if (err) {
                                           console.error(err);
                                           res.status(500);
                                           res.end();
                                       } else {
                                           // user.password = undefined;
                                           res.json(user);
                                           res.end();
                                       }
                                   })
                               } else {
                                   console.error("Token generation failed");
                                   res.status(500);
                                   res.end();
                               }
                           }
                       });
                   } else {
                       res.status(401);
                       res.end();
                   }
               } else {
                   res.status(401);
                   res.end();
               }
           })
           .catch((err) => {
               console.error(err);
               res.status(500);
               res.end();
           })
    });

    app.post('/todo/item', (req, res) => {
       let todo = new Todo(req.body);
       todo.save((err) => {
           if (err) {
               res.status(500);
               res.end();
           } else {
               res.json(todo);
               res.end();
           }
       });
    });

    app.get('/todo/get-item', (req, res) => {
       Todo.find({user: req.query.userId})
           // .populate('user')
           .then((todos) => {
               res.json(todos);
               res.end();
           })
           .catch((err) => {
               res.status(500);
               res.end();
           })
    });



    app.listen(port, () => console.log(`Example app listening on port ${port}!`));
});



