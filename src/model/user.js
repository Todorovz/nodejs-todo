const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        unique: true,
        required: true,
        type: String
    },
    password: {
        required: true,
        type: String,
        select: false
    },
    jwtToken: {
        type: String
    }
});

const User = mongoose.model('user', UserSchema);
module.exports = User;