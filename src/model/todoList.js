const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoListSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    itemName: {
        type: String
    },
    itemStatus: {
        type: String
    }
});

const TodoList = mongoose.model('todoList', TodoListSchema);
module.exports = TodoList;